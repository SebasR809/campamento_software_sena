<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Construir un usuario(email, username y password)
        $u = new User();
        $u -> name = "Sebastian";
        $u -> email = "sebastianramirez0309@gmail.com";
        $u -> password = Hash::make("123456");
        $u -> save();
        
    }
}
