<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Bootcamp;
use File;

class BootcampSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Truncar la tabla bootcamps 
        //Bootcamp::truncate();

        //Leer el archivo bootcamps.json
        $json = File::get("database/_data/bootcamps.json");

        //Convertir el contenido JSON en un arreglo
        $array_bootcamp = json_decode($json);

        //Recorrer ese archivo y por cada bootcamp
        foreach ($array_bootcamp as $b) {        
            //Crear un bootcamp por cada uno
            $n = new Bootcamp();
            $n -> name = $b -> name;
            $n -> description = $b -> description;
            $n -> website = $b -> website;
            $n -> phone = $b -> phone;
            $n -> avg_rating = $b -> avg_rating;
            $n -> avg_cost = $b -> avg_cost;
            $n -> user_id = 1;
            $n -> save();
        }

    }
}
