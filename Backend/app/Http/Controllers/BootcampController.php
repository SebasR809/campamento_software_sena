<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Bootcamp;
//use Illuminate\Support\Facades\Validator;
use App\Http\Requests\storeBootcampRequest;

class BootcampController extends Controller
{
    public function index()
    {
        /*El metodo JSON de los response transmite en formato json
            parametros: datos a transmitir, codigo http del response */
        return response()->json(["success" => true, "data" => Bootcamp::all()], 200);
    }

    public function store(StoreBootcampRequest $request)
    {
        return response()->json(["success" => true, "data" => Bootcamp::create($request->all())], 201);
    }

    public function show($id)
    {
        return response()->json(["success" => true, "data" => Bootcamp::find($id)], 200);
    }

    public function update(Request $request, $id)
    {
        $b = Bootcamp::find($id);
        $b -> update($request -> all());
        return response()->json(["success" => true, "data" => $b], 200);
    }

    public function destroy($id)
    {
        $d = Bootcamp::find($id);
        $d -> delete();
        return response()->json(["success" => true, "data" => $d], 200);;
        //return Bootcamp::destroy($id);
    }
}

