<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class StoreBootcampRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            "name" => 'required|min:5|max:100', 
            "description" => 'required',
            "user_id" => 'required|exists:users,id'
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Nombre obligatorio',
            'description.required' => 'descripción obligatorio',
            'name.min' => 'Caracteres minimos: 5'
        ];
    }
    // Metodo para enviar respuesta con errores de validacion
    protected function failedValidation(Validator $v)
    {
        //Si la validación sea fallida se lanza una excepcion a HTTP
        throw new HttpResponseException(response() -> json(["success" => false, "errors" => $v -> errors()], 422));
        
    }
}
