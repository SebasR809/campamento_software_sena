<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class StoreCourseRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            "title" => 'required|min:10|max:30', 
            "description" => 'required|min:10',
            "weeks" => 'required|integer|max:9',
            "enroll_cost" => 'required|integer',
            "minimum_skill" => 'required|in:Begginer,Intermediate,Advanced,Expert',
            "bootcamp_id" => 'required|exists:bootcamps,id',
        ];
    }
    public function messages()
    {
        return [
            'title.required' => 'Titulo obligatorio',
            'title.min' => 'Minimo de caracteres permitido 10',
            'title.max' => 'Maximo de caracteres permitido 30',
            'description.required' => 'descripción obligatoria',
            'description.min' => 'Minimo de caracteres permitido de la descripción',
            'weeks.required' => 'El campo semanas debe ser obligatorio',
            'weeks.integer' => 'El numero de semanas tiene que ser un numero entero',
            'weeks.max' => 'El máximo de semanas debe ser 9',
            'enroll_cost.required' => 'Costo de inscripción obligatorio',
            'enroll_cost.integer' => 'El costo de inscripción debe ser un valor entero',
            'minimum_skill.required' => 'El nivel debe ser obligatorio',
            'minimum_skill.in' => 'Debe de ser uno de los siguientes niveles: begginer, intermediate, advanced o expert',
            'bootcamp_id.required' => 'El campo id bootcamp debe ser obligatorio'
        ];
    }
    // Metodo para enviar respuesta con errores de validacion
    protected function failedValidation(Validator $v)
    {
        //Si la validación sea fallida se lanza una excepcion a HTTP
        throw new HttpResponseException(response() -> json(["success" => false, "errors" => $v -> errors()], 422));
        
    }
}
